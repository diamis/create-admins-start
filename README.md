## Устновить docker в систему, если еще не установлен

## Создать файл .env в корне проекта из .env.example

## Если еще нет nginxProxy в системе то сначала нужно его нужно установить
1. копируем содержимое папки docker/nginxProxy в папку ${userName}/code/nginxProxy
2. Заходим в нее через консоль
3. Делаем docker-compose up -d

Примечание, nginxProxy должен быть запущен всегда, он проксирует запросы в соответствии с прописанным url в файле .env (REAL_URL)

## Прописать REAL_URL из .env в host файле (/etc/hosts), чтоб запросы проксировались на локальный хост
127.0.0.1 exemple.loc

## Доступ к БД
localhost
5422
логин и пароль из .env файла

## Запуск контейнеров в режиме демона

```bash
docker-compose up -d
```

## Смотреть логи в реалтайме
```bash
docker-compose logs -f
```

## Зайти внутрь контейнера и запустить миграции + сиды

```bash
docker-compose exec app bash
adonis migration:run && adonis seed
```

### Доки

Документация по фрейму
adonisjs.com

### Docs auth
gate_user
gate_user_password
